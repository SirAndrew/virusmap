package com.example.virusmap;

import com.mysql.jdbc.NonRegisteringDriver;

import java.sql.DriverManager;
import java.sql.SQLException;

public class Driver extends NonRegisteringDriver {
    public Driver() throws SQLException {
    }

    static {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        } catch (SQLException var1) {
            throw new RuntimeException("Can't register driver!");
        }
    }
}
