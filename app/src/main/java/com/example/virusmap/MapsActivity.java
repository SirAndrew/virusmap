package com.example.virusmap;

import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import static com.example.virusmap.MyLocationListener.imHere;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener {

    private GoogleMap mMap;
    private static final int MY_PERMISSION_ACCESS_COARSE_LOCATION = 11;
    private static final String TAG = "KEK";
    public LatLng Volgograd = new LatLng(48.7193900,44.5018300);
    public LatLng Polikek = new LatLng(48.713790,44.527972);
    public String Coord1 = "48.7193900;44.5018300";
    public String Coord2 = "48.713790;44.527972";
    public ArrayList <String>  CoordList = new ArrayList<>();
    public connection classcon = new connection();
    private Button buttonshow;
    private Button sendcoords;
    public String uploadCoords;
    public DriverThreadUpload mDriverThreadUpload;
    public DriverThread mDriverThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        buttonshow = findViewById(R.id.button_show);
        sendcoords = findViewById(R.id.send_coords);
        sendcoords.setOnClickListener(this);
        buttonshow.setOnClickListener(this);
        if ( ContextCompat.checkSelfPermission( this, android.Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions( this, new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION  },
                    MY_PERMISSION_ACCESS_COARSE_LOCATION );
        }
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] {android.Manifest.permission.ACCESS_FINE_LOCATION}, 123);
            }
        MyLocationListener.SetUpLocationListener(this);

    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.moveCamera(CameraUpdateFactory.newLatLng(Volgograd));
        CameraPosition camPos = new CameraPosition.Builder()
                .target(Volgograd)
                .zoom(10)
                .bearing(0)
                .tilt(0)
                .build();
        CameraUpdate camUpd3 = CameraUpdateFactory.newCameraPosition(camPos);
        googleMap.animateCamera(camUpd3);
    }
    
    public void drowCircles (ArrayList<String> coordList){
        for (int i=0;i<coordList.size();i++){
            mMap.addCircle(getCircleOptions(coordConvert(coordList.get(i))));
            Log.d(TAG, coordList.get(i));
        }
    }

    public LatLng coordConvert (String coord){
        int pos = coord.indexOf(";");
        LatLng latLng = new LatLng(Double.valueOf(coord.substring(0,pos)),Double.valueOf(coord.substring(pos+1)));
        return latLng;
    }

    public CircleOptions getCircleOptions(LatLng Coord) {
        CircleOptions co = new CircleOptions();
        co.center(Coord);
        co.radius(100);
        co.fillColor(getResources().getColor(R.color.Red80));
        co.strokeColor(Color.RED);
        co.strokeWidth(2.0f);
        return co;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_show:
                mMap.clear();
                mDriverThread= new DriverThread();
                mDriverThread.execute();
                drowCircles(CoordList);
                CoordList.clear();
                break;
            case R.id.send_coords:
                try {
                    uploadCoords = Double.toString(imHere.getLatitude()) + ";" + Double.toString(imHere.getLongitude());
                    Log.d(TAG, uploadCoords);
                    mDriverThreadUpload = new DriverThreadUpload();
                    mDriverThreadUpload.execute();
                }
                catch (NullPointerException e){
                    Toast toast = Toast.makeText(getApplicationContext(),"Геолокация не определена, попробуйте позже", Toast.LENGTH_SHORT);
                    toast.show();
                }
                break;
        }
    }

    class DriverThread extends AsyncTask<String,String,String>{
        Connection conn;
        Statement stmt;
        ResultSet rs;
        String query = "select coords from Data";
        int index=0;
        @Override
        protected String doInBackground(String... strings) {
            try {
                conn = classcon.connect();
                Log.d(TAG,"Соединение установлено");
                stmt = conn.createStatement();
                rs = stmt.executeQuery(query);
                while (rs.next()) {
                    String str = rs.getString(1);
                    CoordList.add(str);
                    index++;
                }

            }
            catch (Exception e){
                Log.e(TAG, "Пошел нахуй", e);
            }
            finally {
                try { conn.close(); } catch(SQLException se) { /*can't do anything */ }
                try { stmt.close(); } catch(SQLException se) { /*can't do anything */ }
                try { rs.close(); } catch(SQLException se) { /*can't do anything */ }

            }
            return null;
        }
    }

    class DriverThreadUpload extends AsyncTask<String,String,String>{
        Connection conn;
        Statement stmt;
        ResultSet rs;
        String query = "insert into sql7329708.Data(coords) values('" + uploadCoords + "');";
        @Override
        protected String doInBackground(String... strings) {
            try {
                conn = classcon.connect();
                Log.d(TAG,"Соединение установлено");
                stmt = conn.createStatement();
                stmt.executeUpdate(query);
                Log.d(TAG,"Загрузка завершена");
            }
            catch (Exception e){
                Log.e(TAG, "Пошел нахуй", e);
            }
            finally {
                try { conn.close(); } catch(SQLException se) { /*can't do anything */ }
                try { stmt.close(); } catch(SQLException se) { /*can't do anything */ }
            }
            return null;
        }
    }

}





